FROM python:3.11-slim
WORKDIR /code

COPY . .

CMD ["python", "./charcter_count.py"]