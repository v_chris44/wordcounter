from character_count import count_characters


def test_character_count():
    res = count_characters('test.txt')
    assert res == 13

def test_character_count():
    res = count_characters('test2.txt')
    assert res == -1